import bpy
import math

from bpy.props import FloatProperty


class SmoothShading(bpy.types.Operator):
    """Activate the Auto Smooth with an 45° angle"""
    bl_idname = "object.smooth_shading"
    bl_label = "Advanced Smooth Shading"
    bl_options = {'REGISTER', 'UNDO'}

    # angle = math.radians(3)
    angle: FloatProperty(
        name='Angle',
        description='Angle to control the smooth shading',
        subtype='ANGLE',
        default=math.degrees(3)
    )

    def execute(self, context):
        sltd_obj = context.selected_objects
        data = bpy.data

        for obj in sltd_obj:
            if obj.type == "MESH":
                bpy.ops.object.shade_smooth()
                data.objects[obj.name].data.use_auto_smooth = True
                mesh_name = data.objects[obj.name].data.name
                bpy.data.meshes[mesh_name].auto_smooth_angle = self.angle

        return {'FINISHED'}

    def invoke(self, context, event):
        user_preferences = bpy.context.preferences
        print("Check addon pref : {0}".format(__package__))
        addon_prefs = user_preferences.addons[__package__]
        if addon_prefs:
            self.angle = addon_prefs.preferences.smooth

        return self.execute(context)
