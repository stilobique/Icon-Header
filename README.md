# Icon Header
This addon add many operator into your 3D View Header.

* **Grid :** Hide or Show the grid
* **Smooth Shading :** This operator automated shading function, make a smooth shading to all selected objects, 
  and activate the auto smooth function with (by default) an angle to 45 degree. This value can be edit on the 
  addon settings.
* **Retopologie Shading :** This operator change the viewport to activate the Retopo shading, it's a X-ary view (to 
  selected object) and the Hidden Wire function enable.

# How to use it
![UI Grid](ressources/icon_header.png)
