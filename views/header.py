import bpy


def STILOBIQUE_HT_viewport(self, context):
    layout = self.layout
    row = layout.row(align=True)

    row.separator()
    row.popover(panel="STILOBIQUE_PT_viewport_sub", text="", text_ctxt="", translate=True, icon='FUND')


class STILOBIQUE_PT_viewport_sub(bpy.types.Panel):
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'HEADER'
    bl_label = "Stilobique Operator"

    def draw(self, context):
        layout = self.layout
        row = layout.row()
        scn = context.scene.icon_header

        row.prop(scn, 'grid_activate', icon='GRID', icon_only=True)
        # icon = 'CURSOR'
        # row.operator("object.center_pivot_mesh_obj", text='', icon=icon)
        row.operator("object.smooth_shading", text='', icon='SHADING_RENDERED')

        # row = layout.row(align=True)
        # icon = 'FORCE_TEXTURE'
        # row.operator("unwrap.uv_checker", text='', icon=icon)
        row.operator("object.retopo_shading", text='', icon='EDITMODE_HLT')


def ui_uv(self, context):
    layout = self.layout
    row = layout.row(align=True)

    icon = 'CURSOR'
    row.operator("unwrap.reset_cursor", text='', icon=icon)
    icon = 'FORCE_TEXTURE'
    row.operator("unwrap.uv_checker", text='', icon=icon)
