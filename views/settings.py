import bpy
import math


class IconHeaderSetting(bpy.types.AddonPreferences):
    bl_idname = "Icon-Header"

    smoothing_intensity: bpy.props.FloatProperty(
        name="Smoothing intensity",
        default=math.radians(45),
        min=0,
        max=math.radians(180),
        subtype='ANGLE',
        )

    def draw(self, context):
        layout = self.layout
        layout.prop(self, "smoothing_intensity")
