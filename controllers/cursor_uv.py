import bpy


class ResetCursor(bpy.types.Operator):
    """Change the cursor location to 0, 0 coordinate and switch the pivot
    use with the cursor"""
    bl_idname = "unwrap.reset_cursor"
    bl_label = "Simple Object Operator"

    def execute(self, context):
        context.space_data.cursor_location[0] = 0
        context.space_data.cursor_location[1] = 0
        context.space_data.pivot_point = 'CURSOR'

        return {'FINISHED'}
