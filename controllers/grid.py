import bpy


class GridControl(bpy.types.Operator):
    """Show or hide the Grid on the 3D/2D active View"""
    bl_idname = "view.grid_control"
    bl_label = "Show or Hide Grid"

    def __init__(self):
        self.id_view = None

    @classmethod
    def poll(cls, context):
        screen = context.screen
        for i, view in enumerate(screen.areas):
            if view.type == 'VIEW_3D':
                return context.screen is not None

    def execute(self, context):
        scn = context.scene.icon_header
        grid_state = scn.grid_activate

        screen = context.screen
        for i, view in enumerate(screen.areas):
            if view.type == 'VIEW_3D':
                self.id_view = i

        scene_view = context.screen.areas[self.id_view].spaces.active.overlay
        scene_view.show_ortho_grid = scene_view.show_floor = scene_view.show_axis_x = scene_view.show_axis_y = \
            scene_view.show_axis_z = grid_state

        return {'FINISHED'}
